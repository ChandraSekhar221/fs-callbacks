const fs = require('fs')

exports.myPromise = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                reject(err)
            } else resolve(data)

        })
    })
}

exports.writeFileFunction = (filePath, data) => {
    fs.writeFile(filePath, data, (err) => { if (err) throw err })
}

exports.appendFileFunction = (filePath1, filePath2) => {
    fs.appendFile(filePath1, filePath2, (err) => { if (err) throw err })
}

exports.deleteFileFunction = (filepathsArray) => {
    filepathsArray.forEach((eachFilePath) => {
        if (eachFilePath.length > 0) {
            fs.unlink(eachFilePath, (err) => { if (err) throw err })
        }
    })
}

