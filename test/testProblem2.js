const path = require('path')
const fs = require('fs')

const test = require('../problem2')

fs.readFile(path.resolve(__dirname, 'lipsum.txt'), 'utf-8', (err, lipsumData) => {
    if (err) throw err
    else {
        test.writefileFunction(path.resolve(__dirname, 'randomfile1.txt'), lipsumData.toUpperCase())
        test.appendFileFunction(path.resolve(__dirname, 'fileNames.txt'), path.resolve(__dirname, 'randomfile1.txt' + '\n'))
        fs.readFile(path.resolve(__dirname, 'randomfile1.txt'), 'utf-8', (err, upperCaseData) => {
            if (err) throw err
            else {
                test.writefileFunction(path.resolve(__dirname, 'randomfile2.txt'), JSON.stringify(upperCaseData.toLowerCase().split('.')))
                test.appendFileFunction(path.resolve(__dirname, 'fileNames.txt'), path.resolve(__dirname, 'randomfile2.txt' + '\n'))
                fs.readFile(path.resolve(__dirname, 'randomfile2.txt'), 'utf-8', (err, splittedData) => {
                    if (err) throw err
                    test.writefileFunction(path.resolve(__dirname, 'randomfile3.txt'), JSON.stringify(JSON.parse(splittedData).sort()))
                    test.appendFileFunction(path.resolve(__dirname, 'fileNames.txt'), path.resolve(__dirname, 'randomfile3.txt'))
                    fs.readFile(path.resolve(__dirname, 'fileNames.txt'), 'utf-8', (err, filePaths) => {
                        if (err) throw err
                        const filePathsArray = filePaths.split('\n')
                        filePathsArray.forEach((eachfile) => {
                            fs.unlink(eachfile, (err) => { if (err) throw err })
                        })
                    })
                })
            }
        })
    }
})
