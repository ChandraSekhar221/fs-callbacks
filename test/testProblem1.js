const path = require('path')

const test = require('../problem1')

test.createADictionary(path.resolve(__dirname,'random'))
test.writeFileFunction(path.resolve(__dirname,'./random/random1.txt'),'Hi Random file1 ')
test.writeFileFunction(path.resolve(__dirname,'./random/random2.txt'),'Hi Random file2 ')
test.writeFileFunction(path.resolve(__dirname,'./random/random3.txt'),'Hi Random file3 ')
test.writeFileFunction(path.resolve(__dirname,'./random/random4.txt'),'Hi Random file4 ')
test.deleteFile(path.resolve(__dirname,'./random/random1.txt'))
test.deleteFile(path.resolve(__dirname,'./random/random2.txt'))
test.deleteFile(path.resolve(__dirname,'./random/random3.txt'))
test.deleteFile(path.resolve(__dirname,'./random/random4.txt'))
test.deleteDirectory(path.resolve(__dirname,'random'))