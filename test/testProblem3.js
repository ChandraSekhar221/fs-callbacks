const test = require('../problem3')

const path = require('path')

const dataFilePath = path.resolve(__dirname,'lipsum.txt')
const fileNamesPath = path.resolve(__dirname,'fileNames2.txt')
const upperCaseFilePath = path.resolve(__dirname,'file1.txt')
const lowerCaseSplittedFilePath = path.resolve(__dirname,'file2.txt')
const sortedFilePath = path.resolve(__dirname,'file3.txt')

test.myPromise(dataFilePath)
.then((lipsumData) => {
    test.writeFileFunction(upperCaseFilePath,lipsumData.toUpperCase())
    test.appendFileFunction(fileNamesPath,upperCaseFilePath+'\n')
    return test.myPromise(upperCaseFilePath)
})
.then((upperCaseData) => {
    let lowerCaseSplittedData = upperCaseData.toLowerCase().split('.')
    test.writeFileFunction(lowerCaseSplittedFilePath,JSON.stringify(lowerCaseSplittedData))
    test.appendFileFunction(fileNamesPath,lowerCaseSplittedFilePath+'\n')
    return test.myPromise(lowerCaseSplittedFilePath)
})
.then((lowerCaseData) => {
    let sortedData = JSON.parse(lowerCaseData).sort()
    test.writeFileFunction(sortedFilePath,JSON.stringify(sortedData))
    test.appendFileFunction(fileNamesPath,sortedFilePath+'\n')
    return test.myPromise(fileNamesPath)
})
.then((filePathsData) => {
    let fileNamesArray = filePathsData.split('\n')
    test.deleteFileFunction(fileNamesArray)
    test.writeFileFunction(fileNamesPath,"",(err) => {if (err) throw err})
})
.catch((err) => { console.log(err)})