const fs = require('fs')

module.exports.createADictionary = (dirPath) => {
    fs.mkdir(dirPath, (err) => { if (err) throw err })
}

module.exports.deleteDirectory = (dirPath) => {
    fs.rmdir(dirPath,(err) => { if (err) throw err})
}

module.exports.writeFileFunction = (filePath, data) => {
    fs.writeFile(filePath, data, (err) => { if (err) throw err })
}

module.exports.deleteFile = (filePath) => {
    fs.unlink(filePath,(err) => { if (err) throw err })
}
